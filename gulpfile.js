const gulp = require('gulp'),
  sass = require('gulp-sass'),
  minifycss = require('gulp-minify-css'),
  uglify = require('gulp-uglify'),
  concat = require('gulp-concat'),
  del = require('del'),
  nunjucks = require('gulp-nunjucks');

const distVersions = ['', '-without-jquery'];
const libsFiles = [
  'src/js/libs/jquery-ui-autocomplete.js',
  'node_modules/leaflet/dist/leaflet.js',
  // could not make it work to import markercluster plugin from typescript / webpack, what a hassle !
  // so instead importing manually
  'node_modules/leaflet.markercluster/dist/leaflet.markercluster.js',
  'node_modules/moment/moment.js',
  'node_modules/moment-range/dist/moment-range.js',
  'node_modules/universal-geocoder/dist/universal-geocoder.min.js',
  'node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.js',
  'node_modules/bootstrap-datepicker/dist/locales/bootstrap-datepicker.en-GB.min.js',
  'node_modules/bootstrap-datepicker/dist/locales/bootstrap-datepicker.fr.min.js',
  'node_modules/bootstrap-datepicker/dist/locales/bootstrap-datepicker.de.min.js',
  'src/js/libs/materialize/**/*.js',
  '!src/js/libs/materialize/unused/**/*.js',
  'src/js/libs/routie.js',  
]
// const scriptsLibs = async () =>
//   gulp.series(concatLibs, concatLibsWithoutJquery)


const concatLibs = () => 
  gulp.src(['node_modules/jquery/dist/jquery.min.js'].concat(libsFiles))
    .pipe(concat('libs.js'))
    .pipe(gulp.dest('web/build'));

const concatLibsWithoutJquery = () => 
  gulp.src(libsFiles)
    .pipe(concat('libs-without-jquery.js'))
    .pipe(gulp.dest('web/build'));

const templates = () =>
  gulp.src(['src/views/**/*.html.njk'])
    .pipe(nunjucks.precompile())
    .pipe(concat('templates.js'))
    .pipe(gulp.dest('web/build'));

const styles = () =>
  gulp.src(['src/scss/**/*.scss'])
    .pipe(sass({ includePaths: ['node_modules']}).on('error', sass.logError))
    .pipe(gulp.dest('web/assets'));

const prodStyles = () =>
  gulp.src('dist/*!(*.min).css')
    .pipe(minifycss())
    .pipe(gulp.dest('dist'));

const concatJs = () =>
  gulp.src([
    'web/build/libs.js',
    'web/build/templates.js',
    'web/gogocarto-src.js'
  ])
    .pipe(concat('gogocarto.js'))
    .pipe(gulp.dest('dist'));

const concatJsWithoutJquery = () =>
  gulp.src([
    'web/build/libs-without-jquery.js',
    'web/build/templates.js',
    'web/gogocarto-src.js'
  ])
    .pipe(concat('gogocarto-without-jquery.js'))
    .pipe(gulp.dest('dist'));

const concatCss = () =>
  gulp.src([
    'web/assets/gogocarto.css',
    'web/assets/images/styles.css'
  ])
    .pipe(concat('gogocarto.css'))
    .pipe(gulp.dest('dist'));

const fontAssets = () =>
  gulp.src(['web/assets/images/fonts/*'])
    .pipe(gulp.dest('dist/fonts/'));

const imageAssets = () =>
  gulp.src(['web/assets/images/*.png', 'web/assets/images/*.svg'])
    .pipe(gulp.dest('dist/images/'));

const markersAssets = () =>
    gulp.src(['web/markers/*.svg'])
      .pipe(gulp.dest('dist/markers/'));

const prodJs = () =>
  gulp.src(['dist/*!(*.min).js'])
    .pipe(uglify())
    .pipe(gulp.dest('dist'));

const cleanDist = () => del(['dist']);

const watchStyles = () => gulp.watch(['src/scss/**/*.scss'], styles);
const watchLibs = () => gulp.watch('src/js/libs/**/*.js', concatLibs);
const watchTemplates = () => gulp.watch('src/views/**/*.njk', templates);

exports.watch = gulp.parallel(watchStyles, watchLibs, watchTemplates);
exports.build = gulp.parallel(styles, concatLibs, concatLibsWithoutJquery, templates);
exports.cleanDist = cleanDist;
exports.dist = gulp.series(gulp.parallel(
                  gulp.series(concatJs, concatJsWithoutJquery, prodJs),
                  gulp.series(concatCss, prodStyles),
                  fontAssets, imageAssets, markersAssets));