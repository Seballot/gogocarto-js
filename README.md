GoGoCartoJs
==========
A javascript component to create terrific cartography ! GoGoCartoJs is the autonomous frontend part of the [GoGoCarto Project](https://gitlab.adullact.net/pixelhumain/GoGoCarto)

![alt text](https://gitlab.com/seballot/gogocarto-js/raw/master/docs/images/desktop.png "Desktop")
.   .   ![alt text](https://gitlab.com/seballot/gogocarto-js/raw/master/docs/images/mobile.png "Mobile")


Discover
-------------------

[Read the online full documentation](https://pixelhumain.github.io/GoGoCartoJs/)

Demo
-----

[Try it Now!](https://pixelhumain.github.io/GoGoCartoJs/web/examples)


How to Use
--------
`npm install gogocarto-js`

Or use the a CDN
```html

<script src="https://unpkg.com/gogocarto-js/dist/gogocarto.js"></script>
<link rel="stylesheet" href="https://unpkg.com/gogocarto-js/dist/gogocarto.css">
```

Install for Developement
----------------------
Want to contribute? check out [how to install locally](docs/how-to-contribute/1-Installation.md) 


Translations
--------
You are welcome to contribute translating the project using [weblate online web interface](https://hosted.weblate.org/projects/gogocarto/javascript-library/) 


Contact
--------
You can contact us at https://chat.lescommuns.org/channel/gogocarto


