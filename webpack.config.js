const path = require('path');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = {
  entry: './src/js/gogocarto.ts',
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: 'ts-loader',
        exclude: /node_modules/,
        include: path.resolve(__dirname, 'src'),
      },
    ],
  },
  plugins: [
    // new BundleAnalyzerPlugin()
  ],
  resolve: {
    extensions: ['.ts', '.js'],
  },
  devtool: 'inline-source-map',
  output: {
    filename: 'gogocarto-src.js',
    // put it in web folder for the webpack autoreload to work
    path: path.resolve(__dirname, 'web'),
    library: 'goGoCarto',
    libraryTarget: 'window',
    libraryExport: 'default'
  },
  mode: "development",
  devServer: {
    static: './web',
  }
};