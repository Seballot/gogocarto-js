help: ## Outputs this help screen
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

build:
	npx webpack
	gulp build

distribute:
	npx webpack
	gulp build
	gulp dist

watch:
	gulp watch

serve:
	gulp watch &
	npx webpack serve --open

lint:
	npx eslint '*/**/*.{js,ts}' --fix

