var taxonomy = {
  "name":"Catégories Principales",
  "unexpandable":true,
  "options":[
    {
      "id":10427,
      "name":"Agriculture & Alimentation",
      "nameShort":"Agricuture",
      "color":"#98A100",
      "softColor":"#808700",
      "icon":"fa fa-transgender-alt",
      "markerShape":"square",
      "markerSize":1,
      "subcategories":[
        {
          "name":"Type",
          "options":[
            {
              "id":10428,
              "name":"Marché",
              "color":"#00537E",
              "softColor":"#22698E",
              "icon":"fa fa-shopping-basket",
              "markerShape":"star",
              "markerSize":3,
              "textHelper":"Marché paysans",
              "useColorForMarker":false
            }
          ],
          "unexpandable":true
        }
      ]
    },
    {
      "id":10517,
      "name":"Voyages",
      "color":"#1E8065",
      "icon":"fa fa-cog",
      "markerSize":3,
    },
    {
      "id":10473,
      "name":"Education & Formation",
      "nameShort":"Education",
      "color":"#00537E",
      "softColor":"#22698E",
      "icon":"fa fa-forward",
      "markerShape":"star",
      "markerSize":2
    }
  ]
}