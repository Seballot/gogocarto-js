var elements = [
  {
    "id": "wd40",

    "name": "GoGo Example",

    "geo": {
      "latitude":46.0252,
      "longitude":-0.0339
    },

    "taxonomy": [ "Market", "Location", "Auto", "Vélo", 10427, 10517, "0dechet", "alternatiba", "ressourcerie", 10433, 10435, 10436 ],
    "tags" : ["Open Source", "Réutilisable", "Awesome!"],

    "address": {
      "streetAddress":"5 rue Edmond Proust",
      "addressLocality":"Chenay",
      "postalCode":"79120",
      "addressCountry":"FR",
      "customFormatedAddress": "Immeuble 4B, 5 Edmond Proust à Chenay, France"
    },

    "friends": {
      "wd41": "GoGo Example2"
    },

    "event_date": "2020-05-17",
    "price": 5,
    "description": "Short Description Je souhaiterai proposer aux citoyens, une initiation à la gravure sur brique de lait. Pour permettre à cet atelier d'être en adéquation avec Alternatiba, le thème serait la création d'une monnaie locale, où chacun pourrait graver ce qu'il aimerait voir apparaitre sur cette monnaie. Une exposition et vente sera proposée à l'intérieur du camion Je souhaiterai proposer aux citoyens, une initiation à la gravure sur brique de lait. Pour permettre à cet atelier d'être en adéquation avec Alternatiba, le thème serait la création d'une monnaie locale, où chacun pourrait graver ce qu'il aimerait voir apparaitre sur cette monnaie. Une exposition et vente sera proposée à l'intérieur du camion Je souhaiterai proposer aux citoyens, une initiation à la gravure sur brique de lait. Pour permettre à cet atelier d'être en adéquation avec Alternatiba, le thème serait la création d'une monnaie locale, où chacun pourrait graver ce qu'il aimerait voir apparaitre sur cette monnaie. Une exposition et vente sera proposée à l'intérieur du camion",
    "descriptionMore": "Un rassemblement était organisé à 13 heures à Paris sous les fenêtres de la ministre de la Santé Agnès Buzyn aux Invalides. Le cortège est ensuite parti de Montparnasse vers 14 heures. Notre reporter Mona Hammoud est sur place. Des représentants de forces politiques étaient présents pour exprimer leur solidarité avec les retraités. La conseillère de Paris, Danielle Simonnet et les députés Adrien Quatennens et Clémentine Autain (La France insoumise) étaient dans le cortège.",
    "email": "example@gogocarto.fr",
    "createdAt": "15/05/2012",
    "commitment": "Short Description Je souhaiterai proposer aux citoyens, une initiation à la gravure sur brique de lait. Pour permettre à cet atelier d'être en adéquation avec Alternatiba, le thème serait la création d'une monnaie locale, où chacun pourrait graver ce qu'il aimerait voir apparaitre sur cette monnaie. Une expos",

    // "image" : "https://images.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.italyguides.it%2Fimages%2Fgridfolio%2Ftaormina%2Ftaormina.02.jpg&f=1",
    "images": [ "images.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.italyguides.it%2Fimages%2Fgridfolio%2Ftaormina%2Ftaormina.02.jpg&f=1", "http://engineering.naukri.com/wp-content/uploads/sites/19/2015/10/open-source.png", "https://images.duckduckgo.com/iu/?u=http%3A%2F%2Fwww.clipartbest.com%2Fcliparts%2FRcA%2FErG%2FRcAErGEei.png&f=1"],
    "files": [
      "https://www.php.net/images/logos/php-logo.svg",
      "www.php.net/images/logos/php-logo.svg"
    ],
    "website": "example.fr",
    "urls": {
      "communecter": "communeter.org",
      "peertube": "framatub.org"
    },
    // "urls": ["http://communeter.org", "http://framatube.org"],
    // "urls": [
    //   {"type": "communecter", "value": "http://communeter.org" },
    //   {"type": "peertube", "value": "http://framatube.org" }
    // ],
    // "url" : "http://communeter.org",
    "openHours": {
      "Mo":"09:00-12:00",
      "Fr":"09:00-11:30 & 5pm to 9pm"
    },
    "vimeoId": 262718584,
    "openHoursString": "Fermé pendant les vacances d'été",
    "reports": [{"type":3, "value":0, "comment":null, "userEmail":"jmstoerkler@gmail.com", "userRole" :0, "createdAt" :"2022-05-10T18:15:30+02:00"}],"contributions": [{"type":4, "status":1, "user":"admin@presdecheznous.fr", "userRole":3, "resolvedMessage":null, "resolvedBy":"", "createdAt":"2017-10-03T21:20:28+02:00", "updatedAt":"2017-10-03T21:20:28+02:00"}]
  },
  {
    "id": "wd41",

    "name": "GoGo Example2",

    "geo": {
      "latitude":46.3352,
      "longitude":-0.0339
    },

    "taxonomy": [ "Agriculture", "Location", "Auto", "Vélo", 10427 ],
    "tags" : ["Open Source", "Réutilisable", "Awesome!"],

    "event_date": "2020-05-18",
    "price": 15,
    "address": {
      "streetAddress":"5 rue Edmond Proust",
      "addressLocality":"Chenay",
      "postalCode":"79120",
      "addressCountry":"FR",
      "customFormatedAddress": "Immeuble 4B, 5 Edmond Proust à Chenay, France"
    },

    "description": "Short Description Je souhaiterai proposer aux citoyens, une initiation à la gravure sur brique de lait. Pour permettre à cet atelier d'être en adéquation avec Alternatiba, le thème serait la création d'une monnaie locale, où chacun pourrait graver ce qu'il aimerait voir apparaitre sur cette monnaie. Une exposition et vente sera proposée à l'intérieur du camion Je souhaiterai proposer aux citoyens, une initiation à la gravure sur brique de lait. Pour permettre à cet atelier d'être en adéquation avec Alternatiba, le thème serait la création d'une monnaie locale, où chacun pourrait graver ce qu'il aimerait voir apparaitre sur cette monnaie. Une exposition et vente sera proposée à l'intérieur du camion Je souhaiterai proposer aux citoyens, une initiation à la gravure sur brique de lait. Pour permettre à cet atelier d'être en adéquation avec Alternatiba, le thème serait la création d'une monnaie locale, où chacun pourrait graver ce qu'il aimerait voir apparaitre sur cette monnaie. Une exposition et vente sera proposée à l'intérieur du camion",
    "descriptionMore": "Un rassemblement était organisé à 13 heures à Paris sous les fenêtres de la ministre de la Santé Agnès Buzyn aux Invalides. Le cortège est ensuite parti de Montparnasse vers 14 heures. Notre reporter Mona Hammoud est sur place. Des représentants de forces politiques étaient présents pour exprimer leur solidarité avec les retraités. La conseillère de Paris, Danielle Simonnet et les députés Adrien Quatennens et Clémentine Autain (La France insoumise) étaient dans le cortège.",
    "email": "example@gogocarto.fr",
    "telephone": "055452545",

    "images": [ "https://images.duckduckgo.com/iu/?u=http%3A%2F%2Fwww.completementflou.com%2Fwp-content%2Fuploads%2F2013%2F01%2Frois-mages-milan-6.jpg&f=1", "https://pbs.twimg.com/profile_images/948024037707415552/HdJJ7vqF_400x400.jpg", "https://images.duckduckgo.com/iu/?u=http%3A%2F%2Fwww.yogazentrum-freiburg.de%2Fimages%2F02c6c298370a71935.jpg&f=1"],

    "website": "https://example.fr",
    "urls": {
      "communecter": "http://communeter.org",
      "peertube": "http://framatub.org"
    },
    // "urls": ["http://communeter.org", "http://framatube.org"],
    // "urls": [
    //   {"type": "communecter", "value": "http://communeter.org" },
    //   {"type": "peertube", "value": "http://framatube.org" }
    // ],
    // "url" : "http://communeter.org",
    "vimeoId": 196157581,
    "openHours": {
      "Mo":"09:00-12:00",
      "Fr":"09:00-11:30 & 5pm to 9pm"
    },
    "openHoursString": "Fermé pendant les vacances d'été",
    
    "subscriberEmails": ["test@gogo.fr"]
  },
  
  {
    "id":"39",
    "name":"Ma maison test1",
    "geo":{"latitude":49.15278,"longitude":0.44473},
    "address":{"streetAddress":"3472 Route de Citeaux", "addressLocality":"Bonn\u00e9table", "postalCode":"72110", "addressCountry":"fr"},
    "createdAt":"2021-09-18T10:44:16+02:00",
    "updatedAt":"2021-09-18T10:44:16+02:00",
    "status":3,"moderationState":4,
    // "categories": ["Habitat"],
    "taxonomy": [ "Education", "Location", "Auto", "Vélo", 10517 ],
    "tags" : ["Open Source", "Réutilisable", "Awesome!"],

   },
  
  {
    "id":"40",
    "name":"Ma maison without categories",
    "geo":{"latitude":47.15278,"longitude":0.44473},
    "address":{"streetAddress":"3472 Route de Citeaux", "addressLocality":"Bonn\u00e9table", "postalCode":"72110", "addressCountry":"fr"},
    "createdAt":"2021-09-18T10:44:16+02:00",
    "updatedAt":"2021-09-18T10:44:16+02:00",
    "status":3,"moderationState":4,
   
    "taxonomy": [],
    "tags" : ["Open Source", "Réutilisable", "Awesome!"],
    /*
    "categoriesFull": [{"id":65, "name":"Habitat", "description":null, "index":0}],
    "contributions": [{"type":0, "status":3, "user":"admin@admin.fr", "userRole":3, "resolvedMessage":"", "resolvedBy":"admin@admin.fr", "createdAt":"2021-09-18T10:44:16+02:00", "updatedAt":"2021-09-18T10:44:16+02:00"}]
    */
  },
  {
    "id":"3B",
    "name":"Ma maison",
    "geo":{"latitude":44.10476968587024,"longitude":-0.5447338212251719},
    "address":{"streetAddress":"3472 Route de Citeaux", "addressLocality":"Bonn\u00e9table", "postalCode":"72110", "addressCountry":"fr"},
    "createdAt":"2021-09-18T10:45:22+02:00",
    "updatedAt":"2021-09-18T12:44:54+02:00",
    "status":8,
    
    "taxonomy": [ "Producer", "Market", "Location", "Auto", "Vélo", 10427 ],
    "tags" : ["Open Source", "Réutilisable", "Awesome!"],
    /*
    "categories": ["Habitat"],
    "categoriesFull": [{"id":65, "name":"Habitat", "description":null, "index":0}], 
    */
   
    // "aggregatedElements": [
    //   {
    //     "id":"37",
    //     "name":"Ma maison",
    //     "geo":{"latitude":48.15276,"longitude":0.44446},
    //     "address":{"streetAddress":"3472 Route de Citeaux", "addressLocality":"Bonn\u00e9table", "postalCode":"72110", "addressCountry":"fr"},
    //     "createdAt":"2021-09-18T10:43:25+02:00","updatedAt":"2021-09-18T10:45:22+02:00",
    //     "status":-7,//"categories": ["Habitat"],
    //     //"categoriesFull": [{"id":65, "name":"Habitat", "description":null, "index":0}],
    //     "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam condimentum neque id interdum dapibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Curabitur non finibus justo. Quisque lobortis pulvinar arcu, id pharetra nibh consectetur non. Curabitur viverra mauris vitae tempus vulputate. Etiam in pellentesque lacus. In bibendum ligula et eros venenatis, eget tincidunt felis efficitur. Donec nec sapien hendrerit, suscipit enim vel, dapibus leo. Duis elementum odio neque, a gravida est vulputate ut. Suspendisse condimentum pharetra ex, vitae maximus diam. Suspendisse potenti. In rutrum ornare metus. Cras lorem neque, bibendum nec semper quis, rhoncus in leo. Aenean convallis augue ut est bibendum fringilla.",
    //     "descriptionMore": "Proin bibendum tellus justo, ut blandit massa facilisis quis. Nunc tempus pellentesque lorem. Ut quis elementum orci. In facilisis hendrerit lectus et tristique. Duis quis purus non augue semper mollis. Nulla euismod sodales neque, id sollicitudin arcu mollis id. Cras malesuada, mi imperdiet semper ullamcorper, tortor odio mattis ante, viverra elementum lacus metus nec enim. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Suspendisse pretium arcu mauris, posuere tincidunt sem suscipit eu.",
    //     "images": [ "https://images.unsplash.com/photo-1471879832106-c7ab9e0cee23?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1073&q=80", "https://images.unsplash.com/photo-1506057213367-028a17ec52e5?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2070&q=80"],
    //     "taxonomy": [ "Location", "Auto", "Vélo", 10473 ],
    //   },
    //   {
    //     "id":"38",
    //     "name":"Ma maison perchée tout en haut de la colline",
    //     "geo":{"latitude":48.15282,"longitude":0.44467},
    //     "address":{"streetAddress":"3472 Route de Citeaux", "addressLocality":"Bonn\u00e9table", "postalCode":"72110", "addressCountry":"fr"},
    //     "createdAt":"2021-09-18T10:43:45+02:00","updatedAt":"2021-09-18T12:44:54+02:00",
    //     "status":-7,//"categories": ["Habitat"],
    //     //"categoriesFull": [{"id":65, "name":"Habitat", "description":null, "index":0}],
    //     "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam condimentum neque id interdum dapibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Curabitur non finibus justo. Quisque lobortis pulvinar arcu, id pharetra nibh consectetur non. Curabitur viverra mauris vitae tempus vulputate. Etiam in pellentesque lacus. In bibendum ligula et eros venenatis, eget tincidunt felis efficitur. Donec nec sapien hendrerit, suscipit enim vel, dapibus leo. Duis elementum odio neque, a gravida est vulputate ut. Suspendisse condimentum pharetra ex, vitae maximus diam. Suspendisse potenti. In rutrum ornare metus. Cras lorem neque, bibendum nec semper quis, rhoncus in leo. Aenean convallis augue ut est bibendum fringilla.",
    //     "descriptionMore": "Proin bibendum tellus justo, ut blandit massa facilisis quis. Nunc tempus pellentesque lorem. Ut quis elementum orci. In facilisis hendrerit lectus et tristique. Duis quis purus non augue semper mollis. Nulla euismod sodales neque, id sollicitudin arcu mollis id. Cras malesuada, mi imperdiet semper ullamcorper, tortor odio mattis ante, viverra elementum lacus metus nec enim. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Suspendisse pretium arcu mauris, posuere tincidunt sem suscipit eu.",
    //     "taxonomy": [ "Location", "Auto", "Vélo", 10517 ],
    //   },
    //   {
    //     "id":"100",
    //     "name":"Ma maison 3",
    //     "geo":{"latitude":48.15279,"longitude":0.44456},
    //     "address":{"streetAddress":"3472 Route de Citeaux", "addressLocality":"Bonn\u00e9table", "postalCode":"72110", "addressCountry":"fr"},
    //     "createdAt":"2021-09-18T10:45:22+02:00",
    //     "updatedAt":"2021-09-18T12:44:54+02:00",
    //     "status":-7,
        
    //     "taxonomy": [ "Location", "Auto", "Vélo", 10427 ],
    //     "tags" : ["Open Source", "Réutilisable", "Awesome!"],
    //     "event_date": "2020-05-18",
    //     "price": 15,
        
    //     "description": "Short Description Je souhaiterai proposer aux citoyens, une initiation à la gravure sur brique de lait. Pour permettre à cet atelier d'être en adéquation avec Alternatiba, le thème serait la création d'une monnaie locale, où chacun pourrait graver ce qu'il aimerait voir apparaitre sur cette monnaie. Une exposition et vente sera proposée à l'intérieur du camion Je souhaiterai proposer aux citoyens, une initiation à la gravure sur brique de lait. Pour permettre à cet atelier d'être en adéquation avec Alternatiba, le thème serait la création d'une monnaie locale, où chacun pourrait graver ce qu'il aimerait voir apparaitre sur cette monnaie. Une exposition et vente sera proposée à l'intérieur du camion Je souhaiterai proposer aux citoyens, une initiation à la gravure sur brique de lait. Pour permettre à cet atelier d'être en adéquation avec Alternatiba, le thème serait la création d'une monnaie locale, où chacun pourrait graver ce qu'il aimerait voir apparaitre sur cette monnaie. Une exposition et vente sera proposée à l'intérieur du camion",
    //     "descriptionMore": "Un rassemblement était organisé à 13 heures à Paris sous les fenêtres de la ministre de la Santé Agnès Buzyn aux Invalides. Le cortège est ensuite parti de Montparnasse vers 14 heures. Notre reporter Mona Hammoud est sur place. Des représentants de forces politiques étaient présents pour exprimer leur solidarité avec les retraités. La conseillère de Paris, Danielle Simonnet et les députés Adrien Quatennens et Clémentine Autain (La France insoumise) étaient dans le cortège.",
    //     "email": "example@gogocarto.fr",
    //     "telephone": "055452545",

    //     "images": [ "https://images.duckduckgo.com/iu/?u=http%3A%2F%2Fwww.completementflou.com%2Fwp-content%2Fuploads%2F2013%2F01%2Frois-mages-milan-6.jpg&f=1", "https://pbs.twimg.com/profile_images/948024037707415552/HdJJ7vqF_400x400.jpg", "https://images.duckduckgo.com/iu/?u=http%3A%2F%2Fwww.yogazentrum-freiburg.de%2Fimages%2F02c6c298370a71935.jpg&f=1"],
    
    //     "website": "https://example.fr",
    //     "urls": {
    //       "communecter": "http://communeter.org",
    //       "peertube": "http://framatub.org"
    //     },
    //     // "urls": ["http://communeter.org", "http://framatube.org"],
    //     // "urls": [
    //     //   {"type": "communecter", "value": "http://communeter.org" },
    //     //   {"type": "peertube", "value": "http://framatube.org" }
    //     // ],
    //     // "url" : "http://communeter.org",
    //     "vimeoId": 196157581,
    //     "openHours": {
    //       "Mo":"09:00-12:00",
    //       "Fr":"09:00-11:30 & 5pm to 9pm"
    //     },
    //     "openHoursString": "Fermé pendant les vacances d'été"

    //   }
    // ],
    
    "event_date": "2020-05-18",
    "price": 15,
    
    "description": "Short Description Je souhaiterai proposer aux citoyens, une initiation à la gravure sur brique de lait. Pour permettre à cet atelier d'être en adéquation avec Alternatiba, le thème serait la création d'une monnaie locale, où chacun pourrait graver ce qu'il aimerait voir apparaitre sur cette monnaie. Une exposition et vente sera proposée à l'intérieur du camion Je souhaiterai proposer aux citoyens, une initiation à la gravure sur brique de lait. Pour permettre à cet atelier d'être en adéquation avec Alternatiba, le thème serait la création d'une monnaie locale, où chacun pourrait graver ce qu'il aimerait voir apparaitre sur cette monnaie. Une exposition et vente sera proposée à l'intérieur du camion Je souhaiterai proposer aux citoyens, une initiation à la gravure sur brique de lait. Pour permettre à cet atelier d'être en adéquation avec Alternatiba, le thème serait la création d'une monnaie locale, où chacun pourrait graver ce qu'il aimerait voir apparaitre sur cette monnaie. Une exposition et vente sera proposée à l'intérieur du camion",
    "descriptionMore": "Un rassemblement était organisé à 13 heures à Paris sous les fenêtres de la ministre de la Santé Agnès Buzyn aux Invalides. Le cortège est ensuite parti de Montparnasse vers 14 heures. Notre reporter Mona Hammoud est sur place. Des représentants de forces politiques étaient présents pour exprimer leur solidarité avec les retraités. La conseillère de Paris, Danielle Simonnet et les députés Adrien Quatennens et Clémentine Autain (La France insoumise) étaient dans le cortège.",
    "email": "example@gogocarto.fr",
    "telephone": "055452545",

    "images": [ "https://images.duckduckgo.com/iu/?u=http%3A%2F%2Fwww.completementflou.com%2Fwp-content%2Fuploads%2F2013%2F01%2Frois-mages-milan-6.jpg&f=1", "https://pbs.twimg.com/profile_images/948024037707415552/HdJJ7vqF_400x400.jpg", "https://images.duckduckgo.com/iu/?u=http%3A%2F%2Fwww.yogazentrum-freiburg.de%2Fimages%2F02c6c298370a71935.jpg&f=1"],

    "website": "https://example.fr",
    "urls": {
      "communecter": "http://communeter.org",
      "peertube": "http://framatub.org"
    },
    // "urls": ["http://communeter.org", "http://framatube.org"],
    // "urls": [
    //   {"type": "communecter", "value": "http://communeter.org" },
    //   {"type": "peertube", "value": "http://framatube.org" }
    // ],
    // "url" : "http://communeter.org",
    "vimeoId": 196157581,
    "openHours": {
      "Mo":"09:00-12:00",
      "Fr":"09:00-11:30 & 5pm to 9pm"
    },
    "openHoursString": "Fermé pendant les vacances d'été"

  }
]
