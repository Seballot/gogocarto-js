var taxonomy = {
  "options": [
    {
      "name":"Agriculture",
      "color":"#3f51b5",
      "icon":"fa fa-graduation-cap",
      // "markerSize": 1,
      "showExpanded": false,
      "subcategories":[
        {
          "name": "Type",
          "expanded": true,
          "displayInInfoBar": false,
          "options": [
            {
              "name":"Market",
              "color":"#e91e63",
              "icon":"fa fa-leaf",
              // "markerSize": 1,
            },
            {
              "name":"Producer",
              "color":"#49887a",
              "icon":"fab fa-angellist",
            },
            {
              "name": "Other"
            }
          ]
        },
        {
          "name": "Type2",
          "expanded": true,
          "isMandatory": false,
          "options": [
            {
              "name": "A",
              "markerShape": "thunderbolt"
            },
            {
              "name": "B",
              "markerShape": "waterdrop-thin-circle"
            }
          ]
        }

      ]
    },
    {
      "name":"Education",
      "color":"#00537E",
      // "icon":"fa fa-graduation-cap"
    },
  ]
}