import { Element } from '../../classes/classes';

import { App } from '../../gogocarto';
declare let $, L: any;
export class Marker {
  element: Element;
  private isAnimating = false;
  // we use leaflet markers to display marker on map. Marker is just a extension of leafletMarker
  private leafletMarker: L.Marker;

  constructor(element: Element) {
    this.element = element;
    this.leafletMarker = L.marker(this.element.position, { riseOnHover: true });
    this.leafletMarker.on('click', (ev) => {
      App.mapManager.handleMarkerClick(this);
    });
    this.leafletMarker.on('mouseover', (ev) => {
      if (!this.isAnimating) this.element.showBigSize();
    });
    this.leafletMarker.on('mouseout', (ev) => {
      if (!this.isAnimating) this.element.showNormalSize();
    });
    this.leafletMarker.setIcon(
      L.divIcon({
        className: 'leaflet-marker-container',
        html: '<span id="marker-' + this.element.id + '" gogo-icon-marker></span>',
      })
    );
  }

  update() {    
    const htmlMarker = App.templateModule.render('marker', {
      element: this.element,
      config: App.config,
      popup: App.templateModule.elementTemplate.renderMarkerPopup(this.element.toDisplay()),
    });

    // save the class because it has been modified by marker cluster adding or
    // removing the "rotate" class
    const oldClassName = (<any>this.leafletMarker)._icon
      ? (<any>this.leafletMarker)._icon.className
      : 'leaflet-marker-container';
    oldClassName.replace('leaflet-marker-icon', '');
    this.leafletMarker.setIcon(L.divIcon({ className: oldClassName, html: htmlMarker }));
  }

  animateDrop() {
    this.isAnimating = true;
    this.domMarker().animate({ top: '-=25px' }, 300, 'easeInOutCubic');
    this.domMarker().animate({ top: '+=25px' }, 250, 'easeInOutCubic', () => {
      this.isAnimating = false;
      this.domMarker().css('top', 'auto');
    });
  }

  showBigSize() {
    this.addClassToLeafletMarker_('big-size');
    const domMarker = this.domMarker();
    domMarker.parent().find('.marker-popup').show();
    domMarker.parent().find('.other-icons-wrapper').css('display', 'flex');
    domMarker.find('.gogo-icon-plus-circle').hide();
  }

  showNormalSize() {
    const domMarker = this.domMarker();
    this.removeClassToLeafletMarker_('big-size');
    if (!App.config.marker.popupAlwaysVisible) domMarker.parent().find('.marker-popup').hide();
    domMarker.parent().find('.other-icons-wrapper').hide();
    domMarker.find('.gogo-icon-plus-circle').show();
  }

  private addClassToLeafletMarker_(classToAdd) {
    this.domMarker().find('.marker-wrapper').addClass(classToAdd);
    this.domMarker().siblings('.marker-popup').addClass(classToAdd);
  }

  private removeClassToLeafletMarker_(classToRemove) {
    this.domMarker().find('.marker-wrapper').removeClass(classToRemove);
    this.domMarker().siblings('.marker-popup').removeClass(classToRemove);
  }

  domMarker() {
    return $('#marker-' + this.element.id);
  }

  getLeafletMarker(): L.Marker {
    return this.leafletMarker;
  }

  getPosition(): L.LatLng {
    return this.leafletMarker.getLatLng();
  }
}
