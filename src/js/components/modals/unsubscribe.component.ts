declare let $: any;
import { Element } from '../../classes/classes';
import { App } from '../../gogocarto';
import { capitalize } from '../../utils/string-helpers';
import { AbstractModalComponent } from './abstract-modal.component';

export class UnsubscribeComponent extends AbstractModalComponent {
  constructor() {
    super('#modal-unsubscribe-element');
    this.ajaxUrl = App.config.features.subscribe.url.replace('subscribe', 'unsubscribe');
  }

  beforeOpen() {
    this.dom.find('.elementName').text(capitalize(this.element.name));
    this.dom.find('#message-error').hide();
    this.dom.find('#mail-error').hide();
    this.dom.find('.input-mail').val('');
  }

  submit() {
    if (!this.element) return;

    const userEmail = this.dom.find('.input-mail').val();

    this.dom.find('#mail-error').hide();
    this.dom.find('#message-error').hide();

    let errors = false;
    if (!userEmail || this.dom.find('.input-mail').hasClass('invalid')) {
      this.dom.find('#mail-error').show();
      errors = true;
    }
    if (!errors) {
      const route = App.config.features.subscribe.url.replace('subscribe', 'unsubscribe');
      const data = {
        elementId: this.element.id,
        userEmail: userEmail,
      };
      this.sendRequest(data);
    } else {
      this.clearLoader();
    }
  }

}
