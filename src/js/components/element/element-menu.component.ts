declare let $: any;

import { AppStates, AppModes } from '../../app.module';
import { Element } from '../../classes/classes';
import { App } from '../../gogocarto';

import { capitalize, slugify } from '../../utils/string-helpers';
import { StampComponent } from './stamp.component';

export class ElementMenuComponent {
  private dom;
  private element: Element;

  constructor(dom: any, element: Element) {
    this.dom = $(dom);
    this.element = element;
    this.initialize();
    this.updateFavoriteIcon();
  }

  updateFavoriteIcon() {
    this.dom.find('.item-add-favorite').toggle(!this.element.isFavorite);
    this.dom.find('.item-remove-favorite').toggle(this.element.isFavorite);
  }

  checkDisplayFullText() {
    if (App.mode == AppModes.List && this.dom.width() > 750) this.showFullTextMenu(true);
    else {
      const fullText = this.dom.width() >= this.dom.find('.menu-element-item:visible').length * 130;
      this.showFullTextMenu(fullText);
    }
  }

  showFullTextMenu(bool: boolean) {
    if (bool) this.dom.addClass('full-text').find('.tooltipped').tooltip('remove');
    else this.dom.removeClass('full-text');
  }

  private initialize() {
    this.dom.find('.tooltipped').tooltip();
    const that = this;

    // STAMPS
    this.dom.find('.item-stamp').each(function () {
      new StampComponent(this, that.element);
    });

    // DELETE
    this.dom.find('.item-delete').click(() => {
      App.deleteComponent.open(this.element);
    });

    // REPORT
    this.dom.find('.item-report').click(() => {
      App.reportComponent.open(this.element);
    });

    // SHOW ON MAP
    this.dom.find('.item-show-on-map').click(() => {
      this.dom.find('.menu-icon').hideTooltip();
      App.setState(AppStates.ShowElement, { id: this.element.id });
    });

    // SHARE
    this.dom.find('.item-share-url').click(() => {
      const modal = $('#modal-share-element');
      modal.find('.modal-footer').attr('option-id', this.element.colorOptionId);

      let url = window.location.origin + window.location.pathname;
      url += App.routerModule.generate('show_element', {
        name: capitalize(slugify(this.element.name)),
        id: this.element.id,
      });

      modal.find('.input-simple-modal').val(url);
      modal.openModal();
    });
    
    // SUBSCRIBE
    this.dom.find('.item-subscribe-element').click(() => {
      const userEmail = App.loginModule.getUserEmail();
      if (userEmail) {
        this.dom.find('.item-subscribe-element .menu-icon').hide();
        this.dom.find('#subscriber-loader').show();
        const route = App.config.features.subscribe.url;
        const data = {
          elementId: this.element.id,
          userEmail: userEmail,
        };
        App.ajaxModule.sendRequest(route, 'post', data, () => App.infoBarComponent.reload() )
      } else {
        App.subscribeComponent.open(this.element);
      }
    });
    this.dom.find('.item-unsubscribe-element').click(() => {
      const userEmail = App.loginModule.getUserEmail();
      if (userEmail) {
        this.dom.find('.item-unsubscribe-element .menu-icon').hide();
        this.dom.find('#unsubscriber-loader').show();
        const route = App.config.features.subscribe.url.replace('subscribe', 'unsubscribe');
        const data = {
          elementId: this.element.id,
          userEmail: userEmail,
        };
        App.ajaxModule.sendRequest(route, 'post', data, () => App.infoBarComponent.reload() )
      } else {
        App.unsubscribeComponent.open(this.element);
      }
    });

    // FAVORITE
    this.dom.find('.item-add-favorite').click(() => {
      App.favoriteModule.addFavorite(this.element.id);
      this.updateFavoriteIcon();
      if (App.mode == AppModes.Map) {
        this.element.marker.update();
        this.element.marker.animateDrop();
      }
    });
    this.dom.find('.item-remove-favorite').click(() => {
      App.favoriteModule.removeFavorite(this.element.id);
      this.updateFavoriteIcon();
      if (App.mode == AppModes.Map) this.element.marker.update();
    });
  }
}
