export class TileLayer {
  name = '';
  url = '';
  attribution: string;
  maxZoom: number;

  constructor(name: string, url: string, attribution?: string, maxZoom?: number) {
    this.name = name;
    this.url = url;
    this.attribution = attribution || '© <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>';
    this.maxZoom = maxZoom || 20
  }
}
