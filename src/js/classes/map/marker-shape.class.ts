import { App } from '../../gogocarto';

export class MarkerShape {
  DEFAULT_SHAPES = [
    { name: "waterdrop", class: "gogo-icon-marker" },
    { name: "waterdrop-thin", class: "fa fa-map-marker" },

    { name: "sign", class: "fas fa-map-signs", iconInside: false },
    { name: "pin", class: "fas fa-map-pin", iconInside: false },
    { name: "thumbtack", class: "gogo-icon-stamp-1", iconInside: false },
    { name: "thunderbolt", class: "fas fa-bolt", iconInside: false },
    { name: "dollar", class: "fas fa-dollar-sign", iconInside: false },
    { name: "exclamation", class: "fas fa-exclamation", iconInside: false },
    { name: "heart", class: "fas fa-heart", iconInside: false },
    { name: "feather", iconInside: false },

    { name: "fun-animal", transform: "translateY(6.5px)", size: 1.15 },
    { name: "fun-animal-2", transform: "translateY(6.5px)", size: 1 },
    { name: "fun-egg", transform: "translateY(3px)", size: 1.15 },
    { name: "fun-flower", transform: "translateY(6px)", size: 1.4 },
    { name: "fun-hair", transform: "translateY(6px)", size: 1.15 },
    { name: "fun-hair-2", transform: "translateY(6px)", size: 1.2 },
    { name: "fun-hat", transform: "translateY(6px)", size: 1.05 },
    { name: "fun-hat-2", transform: "translateY(6px)", size: 1.25 },
    { name: "handsup-flower", transform: "translateY(2px)", size: 1.25 },
    { name: "handsup-hand", transform: "translateY(-8px) translateX(-10px)", size: 1.3 },
    { name: "handsup-heart", transform: "translateY(-1px) translateX(1px)", size: 1.2 },
    { name: "handsup-hotairballoon", transform: "translateY(-6px)", size: 1.25 },
    { name: "handsup-lollipop", transform: "translateY(4px) translateX(1px)", size: 1.05 },
    { name: "handsup-mushroom", transform: "translateY(-6px)", size: 1.2 },
    { name: "handsup-tree", transform: "translateY(4px) translateX(-1px)", size: 1.25 },
    { name: "shape-balloon", transform: "translateY(6.5px)", size: 1 },
    { name: "shape-crest", transform: "translateY(9px)", size: 1 },
    { name: "shape-diamond", transform: "translateY(0px)", size: 1.2 },
    { name: "shape-eye", transform: "translateY(8.5px)", size: 1.5 },
    { name: "shape-flower", transform: "translateY(-15px)", size: 1.4 },
    { name: "shape-flower-2", transform: "translateY(3px)", size: 1.2 },
    { name: "shape-hotairballoon", transform: "translateY(4px)", size: 1 },
    { name: "shape-leaf", transform: "translateY(12px)", size: 1.3 },
    { name: "shape-lollipop", transform: "translateY(4px)", size: 1.05 },
    { name: "shape-mushroom", transform: "translateY(-2px)", size: 1.15 },
    { name: "shape-panel", transform: "translateY(-3px) translateX(-2px)", size: 1.25 },
    { name: "shape-round", transform: "translateY(-3px)", size: 1.15 },
    { name: "shape-round-2", transform: "translateY(-3px)", size: 1.15 },
    { name: "shape-square", transform: "translateY(7px)", size: 1.1 },
    { name: "shape-square-2", transform: "translateY(0px)", size: 1.25 },
    { name: "shape-star", transform: "translateY(-1px)", size: 1.4 },
    { name: "shape-triangle", transform: "translateY(6px)", size: 1.2 },
    { name: "shape-triangleball", transform: "translateY(-5px) translateX(5px)", size: 1.2 },

    { name: "drawing-star", transform: "translateX(-2px) translateY(5px)", size: 1.3 },
    { name: "drawing-apple", size: 1.15 },
    { name: "drawing-cloud", transform: "translateY(3px) translateX(3px)", size: 1.4 },
    { name: "drawing-house", size: 1.1 },
    { name: "drawing-mountain", transform: "translateY(6px)", size: 1.25 },
  ]

  name : string
  class : string
  url : string
  // Whether or not there is space to include another icon inside
  iconInside: true
  // Surround the included icon with a white circle (or something else)
  circle: false
  transform: string
  size: number

  constructor(config) {
    if (typeof config == 'string') {
      // Is it a default shape name
      for (let markerConf of this.DEFAULT_SHAPES) {
        if (config.replace('-circle', '') == markerConf.name)  {
          markerConf['circle'] = config.includes('-circle')
          this.initFromObject(markerConf)
          return
        }
      }
      // Is it an image
      let imagePattern = new RegExp('.*\.(svg|png|jpg|jpeg|gif)')
      if (imagePattern.test(config.toLowerCase())) {
        this.url = config
      } else {
        this.class = config
      }
    } else {
      this.initFromObject(config)
    }
    if (!this.name) {
      this.name = this.class ? this.class : this.url.split('/').slice(-1)[0]
    }
  }

  initFromObject(config) {
    this.name = config.name.replace('-circle', '')
    this.class = config.class
    this.url = config.url
    this.iconInside = config.iconInside
    this.circle = config.circle
    this.transform = config.transform
    this.size = config.size
    if (!this.url && !this.class) {
      this.url = (App?.config?.url?.origin ? App.config.url.origin : '') + `/markers/${this.name}.svg`;
    }
  }
}