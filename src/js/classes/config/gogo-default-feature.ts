export const DEFAULT_FEATURES = {
  listMode: {},
  searchPlace: {},
  searchElements: {},
  searchCategories: {},
  searchGeolocate: {},
  favorite: {},
  share: {},
  layers: {},
  mapdefaultview: {},
  subscribe: {}
};
