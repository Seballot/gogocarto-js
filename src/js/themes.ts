import { App } from './gogocarto';

export function afterTemplateLoaded() {
  if (App.config.theme == 'transiscope') {
    // Color first depth option with square backgorund
    $('#main-option-all > .category-wrapper:first-child .subcategorie-option-item .icon-wrapper').addClass('gogo-bg-color-as')
    $('.category-wrapper .subcategories-wrapper:not(.no-withdrawal) .subcategorie-option-item .icon').addClass(
      'subcategory-icon'
    ).closest('.icon-wrapper').removeClass('gogo-bg-color-as');
  }
}
