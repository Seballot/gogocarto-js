import { App } from '../../gogocarto';
import { Option } from '../../classes/classes'
import { parseArrayNumberIntoString, parseStringIntoArrayNumber } from '../../utils/parser-string-number';
import { slugify } from '../../utils/string-helpers';

declare let $: any;

export class FilterRoutingModule {
  loadFiltersFromString(string: string) {
    const splited = string.split('@');
    const mainOptionSlug = splited[0];
    let mainOptionId, mainOption;
    if (mainOptionSlug == 'all') mainOptionId = 'all';
    else {
      mainOption = App.taxonomyModule.getMainOptionBySlug(mainOptionSlug);
      mainOptionId = mainOption ? mainOption.id : 'all';
    }
    App.filtersComponent.setMainOption(mainOptionId);

    let filtersString: string;

    if (splited.length == 2) {
      filtersString = splited[1];
      filtersString = filtersString.split('+')[0]; // remove + symbol (backward compapatibility)
    } else if (splited.length > 2) {
      console.error('Error spliting in loadFilterFromString');
    }

    const optionsIds = parseStringIntoArrayNumber(filtersString);
    
    if (optionsIds.length > 0) {
      let options = App.taxonomyModule.options.filter(o => optionsIds.includes(o.intId))
      // console.log('Check', "filters from string", options.map(o => o.name));

      // If we want partial taxonomy, hide non selected options
      if (!App.loadFullTaxonomy) {
        App.taxonomyModule.options.forEach(option => {
          if (!optionsIds.includes(option.intId) &&
               option.name != "RootFakeOption" &&
               option.children.length == 0) option.hide()
        })
        if (mainOptionSlug != 'all') $('.main-categories').hide()
      }

      // Uncheck them all
      if (mainOptionSlug == 'all') {
        App.taxonomyModule.taxonomy.toggle(false, false);
      } else {
        if (!mainOption) mainOption = App.taxonomyModule.mainCategory
        mainOption.children.forEach(cat => cat.toggle(false, false))
        App.taxonomyModule.otherRootCategories.forEach(cat => cat.toggle(false, false))
      }

      // Check the ones store in URL
      for (const option of options) {
        option.toggle(true, false);
      }
    }
  }

  getFiltersToString(): string {
    const mainOptionId = App.currMainId;

    let mainOptionName, optionsToConsider;

    if (mainOptionId == 'all' && App.config.menu.showOnePanePerMainOption) {
      mainOptionName = 'all';
      optionsToConsider = []
      // First root categories direct options + all options from other root categories
      App.taxonomyModule.rootCategories.forEach((rootCat, index) => {
        if (index == 0) optionsToConsider = optionsToConsider.concat(rootCat.children)
        else optionsToConsider = optionsToConsider.concat(rootCat.allOptions)
      });
    } else {
      if (App.config.menu.showOnePanePerMainOption) {
        const mainOption = App.taxonomyModule.getCurrMainOption();
        mainOptionName = slugify(mainOption.nameShort);
        // All options from current mainOption + all options form other root categories
        optionsToConsider = mainOption.allChildrenOptions;
        App.taxonomyModule.otherRootCategories.forEach(rootCat => {
          optionsToConsider = optionsToConsider.concat(rootCat.allOptions)
        });
      } else {
        mainOptionName = 'all';
        // All options
        optionsToConsider = App.taxonomyModule.options;
      }
    }

    // console.log(optionsToConsider.map(o=> o.name))

    let checkedOptionsToParse = optionsToConsider.filter((option) => option.isChecked);
    let uncheckOptionsToParse = optionsToConsider.filter((option) => option.isDisabled);
    
    // console.log("CHECKED", checkedOptionsToParse.map(o => o.name))
    // console.log("UNCHECKED", uncheckOptionsToParse.map(o => o.name))

    const checkedIdsParsed = parseArrayNumberIntoString(checkedOptionsToParse.map((option) => option.intId));
    const uncheckedIdsParsed = parseArrayNumberIntoString(uncheckOptionsToParse.map((option) => option.intId));

    if (uncheckedIdsParsed.length == 0) return mainOptionName

    return mainOptionName + '@' + checkedIdsParsed;
  }
}
