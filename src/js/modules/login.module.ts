import { AppModule, AppStates, AppDataType, AppModes } from '../app.module';

import { App } from '../gogocarto';

declare let routie: any, $;

export class LoginModule {
  private roles_: string[];
  private userEmail = '';
  private userGroups: string[];

  constructor($roles: string[] | string, $userEmail = '', $userGroups: string[] | string) {
    this.setRoles($roles);
    this.setUserEmail($userEmail);
    this.setUserGroups($userGroups);
  }

  setRoles($roles: string[] | string) {
    if (typeof $roles == 'string') this.roles_ = [$roles];
    else this.roles_ = $roles;
    if (App) App.config.security.userRoles = this.roles_;
  }

  setUserEmail(userEmail) {
    this.userEmail = userEmail;
    if (App) App.config.security.userEmail = this.userEmail;
  }

  setUserGroups(userGroups) {
    this.userGroups = userGroups;
    if (App) App.config.security.userGroups = this.userGroups;
  }

  getUserEmail() {
    return this.userEmail;
  }

  getRoles() {
    return this.roles_;
  }

  loginAction() {
    if (typeof App.config.security.loginAction == 'function') App.config.security.loginAction();
    else eval(App.config.security.loginAction);
  }
}
