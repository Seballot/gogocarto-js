import { App } from '../../gogocarto';
import { AppDataType, AppStates, AppModes } from '../../app.module';
import { capitalize } from '../../utils/string-helpers';

export class DocumentTitleModule {
  updateDocumentTitle(options: any = {}) {
    // console.log("updateDocumentTitle", App.stateManager.stateElementId);

    let title: string;
    let elementName: string;

    if ((options && options.id) || App.stateManager.stateElementId) {
      const element = App.elementById(App.stateManager.stateElementId);
      if (!element) return;
      elementName = capitalize(element ? element.name : '');
    }

    if (App.dataType == AppDataType.SearchResults) {
      title = App.config.translate('search.for') + ' : ' + App.searchBarComponent.getCurrSearchText();
    } else if (App.mode == AppModes.List) {
      title =
        App.config.translate('list.of') + ' ' +
        App.config.translate('element.plural') +
        this.getLocationAddressForTitle();
    } else {
      switch (App.state) {
        case AppStates.ShowElement:
        case AppStates.ShowElementAlone:
          title = elementName;
          break;
        case AppStates.Normal:
          title =
            App.config.translate('map.of') + ' ' +
            App.config.translate('element.plural') +
            this.getLocationAddressForTitle();
          break;
      }
    }
    if (document.title.includes('-')) title = document.title.split('-')[0] + '- ' + title;
    document.title = title 
  }

  private getLocationAddressForTitle() {
    if (App.geocoder.getLocationAddress()) {
      return ', ' + capitalize(App.geocoder.getLocationAddress());
    }
    return '';
  }
}
