Installation Instructions
=========================

Clone the repository and run:

```shell
npm install --global yarn
yarn
make build
```

Watch assets

```shell
make watch
```

Run the server

```shell
make serve
```

And choose one of the dev file, basic one being `dev-local.html`


Create new distribution files
----------

```shell
make distribute
```

Continue With
-------------

[Coding conventions](2-Coding-conventions.md)
